COMP1005 2022 Tutorial 7
========================


## Background

* [Linked list: Part A](https://www.learn-c.org/en/Linked_lists)
* [Linked List: Part B](https://www.tutorialspoint.com/data_structures_algorithms/linked_list_algorithms.htm)
    * [A demo implementation in C](https://www.tutorialspoint.com/data_structures_algorithms/linked_list_program_in_c.htm)
* [Stack](https://www.tutorialspoint.com/data_structures_algorithms/stack_algorithm.htm)
* [Queue](https://www.tutorialspoint.com/data_structures_algorithms/dsa_queue.htm)

## Exercises

0. Write a program in C to create and display Singly Linked List.  
    ***Expected Output:*** 
    ```
    Input the number of nodes : 3
    Input data for node 1 : 5
    Input data for node 2 : 6
    Input data for node 3 : 7

    Data entered in the list :
    Data = 5
    Data = 6
    Data = 7
    ```

0. Write a program in C to create a singly linked list of n nodes and display it in reverse order.  
    ***Expected Output:*** 
    ```
    Input the number of nodes : 3
    Input data for node 1 : 5
    Input data for node 2 : 6
    Input data for node 3 : 7

    Data entered in the list :
    Data = 5
    Data = 6
    Data = 7

    The list in reverse order :
    Data = 7
    Data = 6
    Data = 5
    ```

0. Write a program in C to insert a new node at the beginning of a Singly Linked List.  
    ***Expected Output:*** 
    ```
    Input the number of nodes : 3
    Input data for node 1 : 5
    Input data for node 2 : 6
    Input data for node 3 : 7

    Data entered in the list :
    Data = 5
    Data = 6
    Data = 7
    
    Input data to insert at the beginning of the list : 4 

    Data after inserted in the list are :  
    Data = 4
    Data = 5
    Data = 6
    Data = 7
    ```

0. Write a program in C to delete first node of Singly Linked List.  
    ***Expected Output:*** 
    ```
    Input the number of nodes : 3
    Input data for node 1 : 2
    Input data for node 2 : 3
    Input data for node 3 : 4

    Data entered in the list :
    Data = 2
    Data = 3
    Data = 4
    
    Data of node 1 which is being deleted is :  2

    Data, after deletion of first node : 
    Data = 3
    Data = 4
    ```


## Extras

0. Write a program in C to insert a new node at the middle of Singly Linked List.  
    ***Expected Output:*** 
    ```
    Input the number of nodes (3 or more) : 4
    Input data for node 1 : 1
    Input data for node 2 : 2
    Input data for node 3 : 3
    Input data for node 4 : 4

    Data entered in the list :
    Data = 1
    Data = 2
    Data = 3
    Data = 4
    
    Input data to insert at the middle of the list : 5 
    Input the position to insert new node : 3 

    Data after inserted in the list are :  
    Data = 1
    Data = 2
    Data = 5
    Data = 3
    Data = 4
    ```

0. Write a program in C to delete a node from the middle of Singly Linked List.  
    ***Expected Output:*** 
    ```
    Input the number of nodes : 3
    Input data for node 1 : 2
    Input data for node 2 : 5
    Input data for node 3 : 8

    Data entered in the list :
    Data = 2
    Data = 5
    Data = 8
    
    Input the position of node to delete : 2

    Data, after deletion: 
    Data = 2
    Data = 8
    ```

0. Try doubly linked lists.  

